from django.test import TestCase
from GameList.models import Jeu

# Create your tests here.

class JeuTestCase(TestCase):
    
    def testsetUp(self):
        Jeu.objects.create(nom="TF2",genre="Action",studio="Valve",date="2007-10-10",image="https://res.cloudinary.com/teepublic/image/private/s--ec3A9OxT--/t_Preview/b_rgb:191919,c_limit,f_jpg,h_630,q_90,w_630/v1539296609/production/designs/3303784_0.jpg")

    def testGet(self):
        jeu=Jeu.objects.create(nom="TF2",genre="Action",studio="Valve",date="2007-10-10",image="https://res.cloudinary.com/teepublic/image/private/s--ec3A9OxT--/t_Preview/b_rgb:191919,c_limit,f_jpg,h_630,q_90,w_630/v1539296609/production/designs/3303784_0.jpg")
        
        assert(jeu.getNom() == "TF2")
        assert(jeu.getGenre() == "Action")
        assert(jeu.getStudio() == "Valve")
        assert(jeu.getDate() == "2007-10-10")

    def testSet(self):
        jeu=Jeu.objects.create(nom="TF2",genre="Action",studio="Valve",date="2007-10-10",image="https://res.cloudinary.com/teepublic/image/private/s--ec3A9OxT--/t_Preview/b_rgb:191919,c_limit,f_jpg,h_630,q_90,w_630/v1539296609/production/designs/3303784_0.jpg")
        
        jeu.setNom("Star Wars Battlefront II")
        jeu.setGenre("Aventure")
        jeu.setStudio("DICE")
        jeu.setDate("2017-11-17")
        jeu.setImage("https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjLya-K4JvlAhVQx4UKHY5iBQcQjRx6BAgBEAQ&url=https%3A%2F%2Fwww.instant-gaming.com%2Ffr%2F1835-acheter-cle-origin-star-wars-battlefront-2%2F&psig=AOvVaw0WEGZyDuL-xp2MWQzJ4zRv&ust=1571142506236606")

        assert(jeu.getNom() == "Star Wars Battlefront II")
        assert(jeu.getGenre() == "Aventure")
        assert(jeu.getStudio() == "DICE")
        assert(jeu.getDate() == "2017-11-17")
        assert(jeu.getImage() == "https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjLya-K4JvlAhVQx4UKHY5iBQcQjRx6BAgBEAQ&url=https%3A%2F%2Fwww.instant-gaming.com%2Ffr%2F1835-acheter-cle-origin-star-wars-battlefront-2%2F&psig=AOvVaw0WEGZyDuL-xp2MWQzJ4zRv&ust=1571142506236606")