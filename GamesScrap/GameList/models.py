from django.db import models
import requests
from bs4 import BeautifulSoup

# Create your models here.

class Jeu(models.Model):
    nom = models.CharField(max_length=64)
    description = models.CharField(max_length=1000)
    genre = models.CharField(max_length=64)
    date = models.CharField(max_length=64)
    image = models.CharField(max_length=1000)
    
    

    def getNom(self):
        return self.nom

    def getDesc(self):
        return self.description

    def getId(self):
        return self.id
    
    def getGenre(self):
        return self.genre
    
    
    def getDate(self):
        return self.date
    
    def getImage(self):
        return self.image


    def setNom(self,arg):
        self.nom=arg
    
    def setDesc(self,arg):
        self.description=arg

    def setId(self,arg):
        self.id=arg


    def setGenre(self,arg):
        self.genre=arg
    
    def setDate(self,arg):
        self.date=arg

    def setImage(self,arg):
        self.image=arg

def ScrapAll():


    links = []
    for i in range(16):
        i+=1
        UNI1 = requests.get("https://www.instant-gaming.com/fr/rechercher/?type[]=steam&page="+ str(i))
        soup1 = BeautifulSoup(UNI1.text, "lxml")

        for a in soup1.find_all('a',{"class": "cover"},href=True):
            if a.text:        
                links.append(a['href'])

    for elem in links:
        UNI = requests.get(elem)
        soup = BeautifulSoup(UNI.text, "lxml")


        rese=[]
        #recupere le titre du jeux 
        for e in soup.findAll("div",{"class": "title"}):
            if len(rese) == 0:        
                rese.append(e.text)

        resd=[]
        #recupere la description 
        for d in soup.findAll("div",{"class": "description"}):
            if len(resd) == 0:        
                resd.append(d.text)

        img = soup.findAll("div",{"class": "screenshots"})
        #prix = soup.findAll("div",{"class": "prices"})

        resc=[]
        prix2 = soup.findAll("div",{"class": "retail"},"span")
        #recupere le prix conseillé du jeu
        for c in soup.findAll("div",{"class": "price"}):
            if len(resc) == 0:        
                resc.append(c.text)

        resa = []
        #recupere la date de sortie
        for a in soup.findAll("span",{"itemprop": "datePublished"}):       
            resa.append(a.text)

        resb=[]
        #recupere l'url de l'image du jeux
        for b in soup.findAll("img",{"class": "picture"},src=True):
            if len(resb) == 0:       
                resb.append(b['src'])

        resf=[]
        #recupere les genres
        for f in soup.findAll("a",{"class": "tag"}):   
            resf.append(f.text)
    
        # print(rese)
        nom=rese[0]
        # print(resd)
        desc=resd[0]
        # print(resc)
        prix=resc[0]
        # print(resb)
        image=resb[0]
        # print(resa)
        date=resa[0]
        # print(resf)
        genre=resf[0]
        if not CheckExist(rese[0]):
            Jeu.objects.create(nom=nom,description=desc,genre=genre,date=date,image=image)
        

    print(len(links))

def CheckExist(string):
    liste=Jeu.objects.all()
    for elem in liste:
        if elem.getNom()==string:
            return True
    return False

def getJeu(id):
    liste=Jeu.objects.all()
    for elem in liste:
        if elem.getId()==id:
            return elem
    return liste[0]    