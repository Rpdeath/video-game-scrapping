from django.apps import AppConfig


class GamelistConfig(AppConfig):
    name = 'GameList'
