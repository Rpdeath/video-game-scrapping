# Video-Game Scrapping

## Instalion du projet
# Notez qu'il faut avoir une version mise à jour de python3 pour une meilleur utilisation.

# Executez les commandes suivantes pour initialisez l'environement virtuel.
    python3 -m venv .

    pip install -r requirements.txt

# Patientez que tous les packages s'installent. Cela peut prendre du temps(Allez donc prendre un café).


## Creation de la BD

# Rendez vous dans le dossier GamesScrap
# Créez un fichier 'db.sqlite3'

# Executez les commandes suivantes
    ./manage.py makemigrations
    ./manage.py migrate
    ./manage.py createsuperuser
# Remplissez les champs qui vous sont demandés

## Lancement du serveur

# Executez la commande
    ./manage.py runserver

# Rendez vous donc sur l'adresse du site qui devrait etre affiché dans le terminal (ici localhost:8000)

# Lors de votre première connexion veuillez vous rendre sur la page localhost:8000/Scrap
# La page devrait charger le temps de créer la liste des jeux

## Gestion de la BD

# Rendez vous sur l'adresse localhost:8000/admin. Renseignez les informations que vous avez utiliser lors de la creation du superuser

# Vous pouvez à présent gérer la BD

## Votre Installation est terminée.




